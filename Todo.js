import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Todo = (props) => {
  const {title} = props;
  return (
    <View>
      <Text>✔️ {title}</Text>
    </View>
  );
};

export default Todo;

const styles = StyleSheet.create({});
