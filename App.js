import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
} from 'react-native';
import Todo from './Todo';

const App = () => {
  const [input, setInput] = useState('');
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.fisrtLabel}>
          <Text>Hello Todo</Text>
        </View>
        <Todo title={"I'm Todo"} />
        <Todo title={'Eat, Code'} />
        <Todo title={'Read, Sleep'} />
        <TextInput style={styles.inputTodo} value={input} onChangeText={(text) => setInput(text)} />
      </SafeAreaView>
    </>
  );
};

export default App;

const styles = StyleSheet.create({
  fisrtLabel: {
    backgroundColor: 'red',
  },
  inputTodo: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
  },
});
